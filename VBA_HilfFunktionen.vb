' Last row of column A and last column of row 1 ###################################################################################################################################
lastRow = Cells(Rows.Count, "A").End(xlUp).Row
lastCol = Cells(1, Columns.Count).End(xlToLeft).Column

' Sort ###################################################################################################################################
Range(Cells(2, "A"), Cells(lastRow, lastCol)).Sort Key1:=Cells(1, toBeSortColumn), Header:=xNo

' Insert empty row between row 1 and 2 ###################################################################################################################################
Range("A2").EntireRow.Insert

' Copy row 1 and insert into row 5 ###################################################################################################################################
Rows(1).Copy Destination:= Rows(5)

' Format as currency ###################################################################################################################################
Range(Cells(2, "B"), Cells(letzteReihe, letzteSpalte)).Style = "Currency"

' Change color of cell  ###################################################################################################################################
Cells(r,c).Interior.ColorIndex = 2

' Traffic Light of Cell A1, which changes every second (Beware that application is freezing (ctrl + break to pause) #######################################
Sub TrafficLight()
    Do While 1
        Cells(1, "A").Interior.ColorIndex = 3 ' Red
        Application.Wait (Now + TimeValue("0:00:01"))
        Cells(1, "A").Interior.ColorIndex = 6 ' Yellow
        Application.Wait (Now + TimeValue("0:00:01"))
        Cells(1, "A").Interior.ColorIndex = 4 ' Green
        Application.Wait (Now + TimeValue("0:00:01"))
    Loop
End Sub


' Sum-Formula  ###################################################################################################################################
Cells(workSheetRow + 1, "B").Formula = "=SUM(" & Range(Cells(2, "B"), Cells((workSheetRow - 1), "B")).Address(False, False) & ")"

' Resize dynamic array with one extra place (Does not work with empty array)
ReDim Preserve myArray(UBound(myArray) + 1) 

	
' Create Button ###################################################################################################################################
Private Sub ButtonErstellen()
	Dim t As Range
    Set t = ActiveSheet.Range(Cells(3, "C"), Cells(3, "C"))
    Set btn = ActiveSheet.Buttons.Add(t.Left, t.Top, t.Width, t.Height)
    With btn
      .OnAction = "DieseArbeitsmappe.buchblattAnlage3" 
      .Caption = "Erstelllen"
      .Name = "Btn" & i
    End With
End Sub


' Create Excel-Table ###################################################################################################################################
Dim tbl As ListObject
Dim rng As Range
Set rng = Range(Cells(1, 1), Cells(letzteReihe, letzteSpalte))
Set tbl = ActiveSheet.ListObjects.Add(xlSrcRange, rng, , xlYes)
tbl.TableStyle = "TableStyleMedium2"

' Delete Sheet if exists and create new empty one ###################################################################################################################################
sheetName = "name"
For Each ws In Worksheets
	If ws.name = sheetName Then
		Application.DisplayAlerts = False
		Sheets(sheetName).Delete
		Application.DisplayAlerts = True
	End If
Next
Worksheets.Add(After:=Worksheets(Worksheets.Count)).Name = sheetName


' Copy worksheet of other excelfile to the end of current workbook  ###################################################################################################################################
Dim sh As Worksheet, twb As Workbook, swb As Workbook
Set twb = ActiveWorkbook
Set swb = Workbooks.Open(fileName)
swb.Worksheets(worksheetName).Copy After:=twb.Sheets(twb.Sheets.Count)
swb.Close (False)


' Open-File-Dialog  ###################################################################################################################################
Private Function openDialogTextFileName()
    Dim fd As Office.FileDialog
    
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
    
    With fd        
        .AllowMultiSelect = False        
        ' Set the title of the dialog box.
        .Title = "Please select the file."        
        ' Clear out the current filters, and add our own.
        .Filters.Clear
        .Filters.Add "Excel-Dateien", "*.xls*"
        .Filters.Add "All Files", "*.*"        
        ' Show the dialog box. If the .Show method returns True, the
        ' user picked at least one file. If the .Show method returns
        ' False, the user clicked Cancel.
        If .Show = True Then
            fileName = .SelectedItems(1)
        Else
            fileName = ""
        End If
    End With
End Function


' Binary search in Worksheet  ###################################################################################################################################
' searchWorksheet:  The worksheet where the function has to search for the searchValue
' searchColum:      The column in the worksheet the function has to search for the searchValue
' searchValue:      The value the function has to search in the searchColumn for
' findColumn:       The column where the to be find value is stored
Private Function binarySearchWorksheet(searchWorksheet As Worksheet, ByVal searchColumn As Long, ByVal searchValue As Long, ByVal findColumn As Long)
    Dim tempSheet As Worksheet
    Set tempSheet = ActiveSheet
    searchWorksheet.Activate

    Dim low As Long
    low = 0
    Dim high As Long
    high = Cells(Rows.Count, searchColumn).End(xlUp).Row
    
    Dim rowIndex As Long
    Dim result As Boolean
    Dim compareValue As Long
    binarySearchWorksheet = ""
    Do While low <= high
        rowIndex = (low + high) / 2
        compareValue = Cells(rowIndex, searchColumn)
        If searchValue = compareValue Then
            arrayFind = True
            binarySearchWorksheet = Cells(rowIndex, findColumn)
            Exit Do
        ElseIf searchValue < compareValue Then
            high = (rowIndex - 1)
        Else
            low = (rowIndex + 1)
        End If
    Loop
    
    tempSheet.Activate
End Function


' Create Combobox and set function to be called at changes ###################################################################################################################################
Sub createComboBoxAtCell(zelle As Range)
    Set curCombo = ActiveSheet.Shapes.AddFormControl(xlDropDown, Left:=zelle.Left, Top:=zelle.Top, Width:=zelle.Width, Height:=zelle.Height)
    With curCombo
           '.ControlFormat.DropDownLines = 2
           .name = "bemerkung" & cmbIndex
           ' At items of specific row in worksheet
            For i = 1 To anzBemerkungen
                .ControlFormat.AddItem Sheets("Bemerkungen").Cells(i, "A")
            Next i
           .OnAction = "DieseArbeitsmappe.fillCell"           
           .ControlFormat.ListIndex = 1
           .ControlFormat.LinkedCell = zelle.Address
    End With
    zelle.Value = curCombo.ControlFormat.List(curCombo.ControlFormat.Value)

    cmbIndex = cmbIndex + 1
End Sub


Private Sub fillCell()
    Dim shp As Shape
    Dim chk As ControlFormat
    
    Set shp = ActiveSheet.Shapes(Application.Caller)
    Set chk = shp.ControlFormat
    Range(chk.LinkedCell).Value = chk.List(chk.Value)
End Sub

' Assign headlines to columnNumbers ###################################################################################################################################
Private Type columnType
     headline As String
     number As Integer
End Type

Private Function searchHeadline(tempCol As columnType)
    Dim lastColumn
    lastColumn = Cells(1, Columns.Count).End(xlToLeft).Column
        
    For i = 1 To lastColumn
        If Cells(1, i) = tempCol.headline Then
            tempCol.number = i
        End If
    Next i
    If tempCol.number = 0 Then
        Debug.Print "Did not find column with headline: " + tempCol.headline
    End If
End Function

' Create E-Mails (and send them) ################
Sub createEmail(recipient As String, cc As String, subject As String, htmlMessage As String)
    Set MyOutApp = CreateObject("Outlook.Application")
    Set MyMessage = MyOutApp.CreateItem(0)
    With MyMessage
        .To = recipient
        If cc <> "" Then
            .cc = cc
        End If
        .subject = subject
        .HTMLBody = htmlMessage
        .Display ' Show E-Mail
        ' .Send ' Finally sends E-Mail
    End With
    Set MyMessage = Nothing 'MyOutApp.CreateItem(0)
    Set MyOutApp = Nothing 'CreateObject("Outlook.Application")
End Sub